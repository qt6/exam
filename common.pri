CONFIG += c++17

ROOT_DIR = $$PWD
COMMON_DIR = $$ROOT_DIR/common
BUILD_DIR = $$ROOT_DIR/build

DESTDIR = $$BUILD_DIR/Exam

INCLUDEPATH += $$COMMON_DIR
DEPENDPATH += $$COMMON_DIR

HEADERS += \
        $$COMMON_DIR/answersinfo.h \
        $$COMMON_DIR/jsoninfo.h \
        $$COMMON_DIR/questionsinfo.h

SOURCES += \
        $$COMMON_DIR/answersinfo.cpp \
        $$COMMON_DIR/jsoninfo.cpp \
        $$COMMON_DIR/questionsinfo.cpp


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    $$COMMON_DIR/resource.qrc

win32: RC_ICONS = $$COMMON_DIR/Resources/icons/app.ico

Release: win32: QMAKE_POST_LINK += windeployqt --qmldir $$[QT_INSTALL_QML] $$DESTDIR
