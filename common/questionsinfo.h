#ifndef QUESTIONSINFO_H
#define QUESTIONSINFO_H

#include "jsoninfo.h"

class AnswersInfo;

class QuestionsInfo : public JsonInfo
{
    Q_OBJECT
public:
    explicit QuestionsInfo(QObject *parent = nullptr);

    virtual void read(const QJsonObject &obj) override;
    virtual void write(QJsonObject &obj) override;

    void setQuestion(const QString &text);
    QString question() const;

    void setCategory(const QString &cat);
    QString category() const;


    void clearAnswers();
    QList <AnswersInfo*> answers() const;
    QList <AnswersInfo*> randomAnswers() const;
    void setAnswer(int index, AnswersInfo *info);
    void addAnswer(AnswersInfo *info = nullptr);

private:

    QString m_question;
    QString m_category;
    QList <AnswersInfo*> m_answers;
};

#endif // QUESTIONSINFO_H
