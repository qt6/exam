#ifndef ANSWERSINFO_H
#define ANSWERSINFO_H

#include "jsoninfo.h"

class AnswersInfo : public JsonInfo
{
    Q_OBJECT
public:
    explicit AnswersInfo(QObject *parent = nullptr);

    virtual void read(const QJsonObject &obj) override;
    virtual void write(QJsonObject &obj) override;

    void setAnswer(const QString &text);
    QString answer() const;

    void  setIsRight(bool isRight);
    bool isRight() const;

    void  setIsChecked(bool isChecked);
    bool isChecked() const;


private:

    QString m_answer;
    bool m_isRight;
    bool m_isChecked;

};

#endif // ANSWERSINFO_H
