#include "questionsinfo.h"
#include "answersinfo.h"

#ifdef QT_DEBUG
#include <QDebug>
#endif

#include <QJsonObject>
#include <QJsonArray>

#include <QRandomGenerator>

QuestionsInfo::QuestionsInfo(QObject *parent)
    : JsonInfo{parent}
{

}

void QuestionsInfo::read(const QJsonObject &obj)
{
    m_question = obj["Question"].toString();
    m_category = obj["Category"].toString();

    auto answersArr = obj["Answers"].toArray();

    m_answers.clear();

    for(int index=0;index<answersArr.size();++index){
        AnswersInfo *answerInfo=new AnswersInfo;

        answerInfo->read(answersArr.at(index).toObject());

        m_answers.append(answerInfo);
    }
}

void QuestionsInfo::write(QJsonObject &obj)
{
    obj["Question"] = m_question;
    obj["Category"] = m_category;

    QJsonArray answersArr;


    for(auto answer:m_answers){
        QJsonObject answerObj;
        answer->write(answerObj);

        answersArr.append(answerObj);
    }



    obj["Answers"]=answersArr;


}

void QuestionsInfo::setQuestion(const QString &text)
{
    if(m_question.compare(text))
        m_question = text;
}

QString QuestionsInfo::question() const
{
    return m_question;
}

void QuestionsInfo::setCategory(const QString &cat)
{
    if(m_category.compare(cat))
        m_category = cat;
}

QString QuestionsInfo::category() const
{
    return m_category;
}

void QuestionsInfo::clearAnswers()
{
    m_answers.clear();
}

QList<AnswersInfo *> QuestionsInfo::answers() const
{
    return m_answers;
}

QList<AnswersInfo *> QuestionsInfo::randomAnswers() const
{
    QList<AnswersInfo *> tmpAnswer = m_answers;
    QList<AnswersInfo *> outAnswer;

    while(tmpAnswer.size())
    {
        auto randomIndex = QRandomGenerator::global()->bounded(tmpAnswer.size());

        outAnswer.append(tmpAnswer.takeAt(randomIndex));
    }


    return outAnswer;

}

void QuestionsInfo::setAnswer(int index, AnswersInfo *info)
{
    if(index < 0)
        return;
    else if(index>=0 && index < m_answers.size())
        m_answers.replace(index, info);
    else
        addAnswer(info);
}

void QuestionsInfo::addAnswer(AnswersInfo *info)
{
    if(!info)
        info=new AnswersInfo();

    m_answers.append(info);
}
