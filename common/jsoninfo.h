#ifndef JSONINFO_H
#define JSONINFO_H

#include <QObject>

class QJsonObject;

class JsonInfo : public QObject
{
    Q_OBJECT
public:
    explicit JsonInfo(QObject *parent = nullptr);

   virtual void read(const QJsonObject &obj) = 0;
   virtual void write(QJsonObject &obj) = 0;

};

#endif // JSONINFO_H
