#include "answersinfo.h"

#include <QJsonObject>

AnswersInfo::AnswersInfo(QObject *parent)
    : JsonInfo{parent}
{
    m_isRight = false;
    m_isChecked = false;
}

void AnswersInfo::read(const QJsonObject &obj)
{
    m_answer = obj["Answer"].toString();
    m_isRight = obj["IsRight"].toBool();
}

void AnswersInfo::write(QJsonObject &obj)
{
    obj["Answer"] = m_answer;
    obj["IsRight"] = m_isRight;
}

void AnswersInfo::setAnswer(const QString &text)
{
    if(m_answer.compare(text)){
        m_answer = text;
    }
}

QString AnswersInfo::answer() const
{
    return m_answer;
}

void AnswersInfo::setIsRight(bool isRight)
{
    if(m_isRight != isRight){
        m_isRight = isRight;
    }
}

bool AnswersInfo::isRight() const
{
    return m_isRight;
}

void AnswersInfo::setIsChecked(bool isChecked)
{
    m_isChecked = isChecked;
}

bool AnswersInfo::isChecked() const
{
    return m_isChecked;
}
